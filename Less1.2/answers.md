#Ответы на задачи:

+ С точки зрения процесса при запуске любой команды (к примеру той же ls -an) bash делает обращение к системному вызову fork() - который клонирует запрос - в котором выполняется системный вызов exec () - который и запускает программу, после того как программа отработала - она посылает SIGCHLD в родительский процесс. Там его ждет системный вызов wait() который уже ждет правильного завершения процесса. 

+ Вывод ps по заданию:
***
```
root@streamer-main:/home/cilkee# ps ax -o pid,user,cpu,vsz,tt,comm | head
    PID USER     CPU    VSZ TT       COMMAND
      1 root       - 170864 ?        systemd
      2 root       -      0 ?        kthreadd
      3 root       -      0 ?        rcu_gp
      4 root       -      0 ?        rcu_par_gp
      6 root       -      0 ?        kworker/0:0H-kblockd
     10 root       -      0 ?        mm_percpu_wq
     11 root       -      0 ?        ksoftirqd/0
     12 root       -      0 ?        rcu_sched
     13 root       -      0 ?        migration/0
```
***

+ Выводы по последнему таску:
***
```
root@streamer-main:/home/cilkee# sleep infinity &
[1] 19049
root@streamer-main:/home/cilkee# ps
    PID TTY          TIME CMD
  18943 pts/1    00:00:00 sudo
  18944 pts/1    00:00:00 su
  18945 pts/1    00:00:00 bash
  19049 pts/1    00:00:00 sleep
  19050 pts/1    00:00:00 ps
root@streamer-main:/home/cilkee# ps | grep sleep
  19049 pts/1    00:00:00 sleep
root@streamer-main:/home/cilkee# kill -9 19049
root@streamer-main:/home/cilkee# ps | grep sleep
[1]+  Killed                  sleep infinity
root@streamer-main:/home/cilkee# ps
    PID TTY          TIME CMD
  18943 pts/1    00:00:00 sudo
  18944 pts/1    00:00:00 su
  18945 pts/1    00:00:00 bash
  19056 pts/1    00:00:00 ps
root@streamer-main:/home/cilkee# 
```
***

