
### Описание задания:

Заставить работать **ls** и **cp** 

см Видео выше.

Вывод консоли в ответ.

***
```
root@jundev:/home/cilkee# chroot Less3
bash-5.1# ls
ls: error while loading shared libraries: libselinux.so.1: cannot open shared object file: No such file or directory
bash-5.1# cp
cp: error while loading shared libraries: libselinux.so.1: cannot open shared object file: No such file or directory
bash-5.1# exit
exit
root@jundev:/home/cilkee# ldd Less3/bin/cp
	linux-vdso.so.1 (0x0000ffff90104000)
	libselinux.so.1 => /lib/aarch64-linux-gnu/libselinux.so.1 (0x0000ffff90050000)
	libacl.so.1 => /lib/aarch64-linux-gnu/libacl.so.1 (0x0000ffff90030000)
	libattr.so.1 => /lib/aarch64-linux-gnu/libattr.so.1 (0x0000ffff90010000)
	libc.so.6 => /lib/aarch64-linux-gnu/libc.so.6 (0x0000ffff8fe60000)
	/lib/ld-linux-aarch64.so.1 (0x0000ffff900cb000)
	libpcre2-8.so.0 => /lib/aarch64-linux-gnu/libpcre2-8.so.0 (0x0000ffff8fdc0000)
root@jundev:/home/cilkee# cp /lib/aarch64-linux-gnu/libselinux.so.1 Less3/lib/
root@jundev:/home/cilkee# cp /lib/aarch64-linux-gnu/libacl.so.1 Less3/lib
root@jundev:/home/cilkee# cp /lib/aarch64-linux-gnu/libattr.so.1 Less3/lib/
root@jundev:/home/cilkee# cp /lib/aarch64-linux-gnu/libc.so.6 Less3/lib/
root@jundev:/home/cilkee# cp /lib/ld-linux-aarch64.so.1 Less3/lib/
root@jundev:/home/cilkee# cp /lib/aarch64-linux-gnu/libpcre2-8.so.0 Less3/lib/
root@jundev:/home/cilkee# chroot Less3
bash-5.1# cp
cp: missing file operand
Try 'cp --help' for more information.
bash-5.1# exit
exit
root@jundev:/home/cilkee# ldd Less3/bin/ls
	linux-vdso.so.1 (0x0000ffffa96a1000)
	libselinux.so.1 => /lib/aarch64-linux-gnu/libselinux.so.1 (0x0000ffffa95f0000)
	libc.so.6 => /lib/aarch64-linux-gnu/libc.so.6 (0x0000ffffa9440000)
	/lib/ld-linux-aarch64.so.1 (0x0000ffffa9668000)
	libpcre2-8.so.0 => /lib/aarch64-linux-gnu/libpcre2-8.so.0 (0x0000ffffa93a0000)
root@jundev:/home/cilkee# cp /lib/aarch64-linux-gnu/libselinux.so.1 Less3/lib/
root@jundev:/home/cilkee# cp /lib/aarch64-linux-gnu/libc.so.6 Less3/lib/
root@jundev:/home/cilkee# cp /lib/ld-linux-aarch64.so.1 Less3/lib/
root@jundev:/home/cilkee# cp /lib/aarch64-linux-gnu/libpcre2-8.so.0 Less3/lib/
root@jundev:/home/cilkee# chroot Less3
bash-5.1# ls
bin  lib
bash-5.1# cp lib/l
ld-linux-aarch64.so.1  libacl.so.1            libattr.so.1           libc.so.6              libpcre2-8.so.0        libselinux.so.1        libtinfo.so.6          
bash-5.1# cp lib/l
ld-linux-aarch64.so.1  libacl.so.1            libattr.so.1           libc.so.6              libpcre2-8.so.0        libselinux.so.1        libtinfo.so.6          
bash-5.1# cp lib/libacl.so.1 lib/librararara
bash-5.1# ls lib/
ld-linux-aarch64.so.1  libacl.so.1  libattr.so.1  libc.so.6  libpcre2-8.so.0  librararara  libselinux.so.1  libtinfo.so.6
bash-5.1# 
```
***
